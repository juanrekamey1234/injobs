validateInputs = () => {
    const dataCheckElement = document.getElementById('checkTerms')
    const nameUser = document.getElementById('nameUserElement').value
    const passwordUser = document.getElementById('passwordUserElement').value
    const emailUser = document.getElementById('emailUserElement').value
    const phoneUser = document.getElementById('phoneUserElement').value
    const addressUser = document.getElementById('addressUserElement').value

    !dataCheckElement.checked || nameUser === '' || passwordUser === '' || emailUser === '' || phoneUser === '' || addressUser === '' ?
        alert('Nesecitas llenar todos los datos y aceptar los terminos y condiciones')
        :
        writeUserDataInFirebase(nameUser, passwordUser, emailUser, phoneUser, addressUser)
  }
  
  writeUserDataInFirebase = (nameUser, passwordUser, emailUser, phoneUser, addressUser) => {
    document.getElementById('nameUserElement').value = ''
    document.getElementById('passwordUserElement').value = ''
    document.getElementById('emailUserElement').value = ''
    document.getElementById('phoneUserElement').value = ''
    document.getElementById('addressUserElement').value = ''
    
    firebase.database().ref('buyers/').push({
        nameUser: nameUser,
        passwordUser: passwordUser,
        emailUser: emailUser,
        phoneUser: phoneUser,
        addressUser: addressUser
    });
  }